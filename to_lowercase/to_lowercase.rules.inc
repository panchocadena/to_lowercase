<?php
/**
* Implement hook_rules_action_info()
* Declare any meta data about actions for Rules
*/

function to_lowercase_rules_action_info() {
 $actions = array(
  'to_lowercase_action' => array(
    'label' => t('Convert to lowercase a TEXT'),
    'group' => t('cadena'),

    'parameter' => array(

        'text' => array(
                'type' => 'text',
                'label' => t('$text-variable-token to be lowercase'),
                ),
    ),


    'provides' => array(
      'text_lower_case' => array(
        'type' => 'text',
        'label' => t('text in lower case'),
      ),
    ),
  ),
);
  return $actions;
}

// rules function numero a texto
function to_lowercase_action($text) {
  $text_lower_case = array();
  $text_lower_case = strtolower($text);

  return array (
  'text_lower_case' => $text_lower_case);
}
